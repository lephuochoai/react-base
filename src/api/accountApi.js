import axiosClient from './axiosClient';

const accountApi = {
  login: (payload) => axiosClient.post('/login', payload),

  register: (payload) => axiosClient.post('/register', payload),
};

export default accountApi;
