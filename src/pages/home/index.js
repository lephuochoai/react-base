import React from 'react';
import { withNamespaces } from 'react-i18next';

function Home({ t, i18n }) {
  return (
    <div>
      {t('home.txt_1')}
    </div>
  );
}

export default withNamespaces()(Home);
