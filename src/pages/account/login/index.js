import { DoubleRightOutlined, LockOutlined, UserOutlined } from '@ant-design/icons';
import Button from 'components/button';
import Field from 'components/field';
import Input from 'components/input';
import InputPassword from 'components/input-password';
import React from 'react';
import { useForm } from 'react-hook-form';
import { Link } from 'react-router-dom';
import './style.less';

function Login() {
  const { control, handleSubmit, errors } = useForm();
  const onSubmit = (data) => console.log(data);

  return (
    <div className="page-login">
      <div className="form-login">
        <div className="style-top">
          <div className="left" />
          <div className="right" />
        </div>
        <div className="title">User Login</div>
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="fields">
            <Field
              component={Input}
              className="field"
              placeholder="Enter username"
              name="username"
              errors={errors}
              prefix={<UserOutlined />}
              control={control}
              defaultValue=""
              rules={{
                required: true,
                maxLength: {
                  value: 255, message: 'Max length 255 charactor',
                },
              }}
            />
          </div>
          <div className="fields">
            <Field
              component={InputPassword}
              className="field"
              defaultValue=""
              placeholder="Enter password"
              name="password"
              prefix={<LockOutlined />}
              control={control}
              errors={errors}
              rules={{
                required: true,
                minLength: {
                  value: 6,
                  message: 'Min length 6 charactor',
                },
                maxLength: {
                  value: 255,
                  message: 'Max length 255 charactor',
                },
              }}
            />
          </div>
          <Button htmlType="submit" className="btn-login" type="primary" block>Login</Button>
          <div className="forgot-password">Forgot password</div>
          <div className="sign-up">
            <Link to="/register">
              Create your account
              <DoubleRightOutlined className="icon" />
            </Link>
          </div>
        </form>
      </div>
    </div>
  );
}

export default Login;
