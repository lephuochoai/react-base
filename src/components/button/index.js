import React from 'react';
import { Button } from 'antd';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import './style.less';

const ButtonCustom = ({ className, children, ...props }) => (
  <Button
    className={classnames(className, 'button-custom')}
    {...props}
  >
    {children}
  </Button>
);

ButtonCustom.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
};

export default ButtonCustom;
