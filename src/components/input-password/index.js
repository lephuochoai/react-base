import React from 'react';
import { Input } from 'antd';
import classnames from 'classnames';
import PropTypes from 'prop-types';

import './style.less';

const InputPasswordCustom = ({ className, children, ...props }) => (
  <Input.Password
    className={classnames(className, 'input-custom')}
    {...props}
  >
    {children}
  </Input.Password>
);

InputPasswordCustom.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
};

export default InputPasswordCustom;
