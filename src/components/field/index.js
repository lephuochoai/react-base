import React from 'react';
import { Controller } from 'react-hook-form';
import PropTypes from 'prop-types';

function Field({
  errors, component, control, name, rules, ...props
}) {
  return (
    <>
      <Controller
        {...props}
        as={component}
        className="field"
        name={name}
        control={control}
        rules={rules}
      />
      {errors[name] && <div className="error-color">{errors[name]?.message || errors[name]?.type}</div>}
    </>
  );
}

Field.propTypes = {
  errors: PropTypes.object.isRequired,
  component: PropTypes.func.isRequired,
  control: PropTypes.object.isRequired,
  name: PropTypes.string.isRequired,
  rules: PropTypes.object.isRequired,
};

export default Field;
