import React from 'react';
import { withNamespaces } from 'react-i18next';
import classnames from 'classnames';
import Container from 'components/container';
import CONSTANTS from 'constants';
import Storage from 'utils/storage';
import viFlag from 'resources/images/flags/vi.svg';
import enFlag from 'resources/images/flags/en.svg';
import jpFlag from 'resources/images/flags/jp.svg';

import './style.less';

function Header({ i18n }) {
  // const currentLanguage

  const getFlag = (code) => {
    switch (code) {
      case 'vi':
        return viFlag;
      case 'en':
        return enFlag;
      default:
        return jpFlag;
    }
  };

  const onChangeLanguage = (lang) => {
    i18n.changeLanguage(lang);
    Storage.set('CURRENT_LANGUAGE', lang);
  };

  return (
    <div className="header-box">
      <Container>
        <div className="header">
          <div className="languages">
            {CONSTANTS.LANGUAGES.map((language) => (
              <img
                key={language.code}
                src={getFlag(language.code)}
                className={classnames('flag', {
                  active: i18n.language === language.code
                })}
                onClick={() => onChangeLanguage(language.code)}
                alt=""
              />
            ))}
          </div>
          <div className="logo">
            Logo
          </div>
          <div className="auth">
            Auth
          </div>
        </div>
      </Container>
    </div>
  );
}

export default withNamespaces()(Header);
