import React from 'react';
import { Input } from 'antd';
import classnames from 'classnames';
import PropTypes from 'prop-types';

import './style.less';

const InputCustom = ({ className, children, ...props }) => (
  <Input
    className={classnames(className, 'input-custom')}
    {...props}
  >
    {children}
  </Input>
);

InputCustom.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
};

export default InputCustom;
