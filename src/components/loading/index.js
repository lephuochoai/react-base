import React from 'react';
import './style.less';

function Loading() {
  return (
    <div className="loading-component">
      <div className="lds-ripple" />
      <div className="txt-loading">Loading</div>
    </div>
  );
}

export default Loading;
