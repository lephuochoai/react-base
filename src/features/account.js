import { createSlice } from '@reduxjs/toolkit';

const accountSlice = createSlice({
  name: 'account',
  initialState: {
    submitting: null,
    error: null,

    token: '',
    infoUser: {},
    role: null,
  },
  reducers: {
    login(state, action) {
      const { token, infoUser, role } = action.payload;
      state.token = token;
      state.infoUser = infoUser;
      state.role = role;
    },
  },
});

export default accountSlice.reducer;
export const {
  login,
} = accountSlice.actions;
