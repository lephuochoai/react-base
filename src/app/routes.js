import PropTypes from 'prop-types';
import React, { lazy, Suspense } from 'react';
import {
  BrowserRouter as Router,
  Redirect, Route, Switch,
} from 'react-router-dom';
import LocalStorage from 'utils/storage';
import Loading from 'components/loading';
import Header from 'components/header';

const Home = lazy(() => import('pages/home'));
const Login = lazy(() => import('pages/account/login'));
const Register = lazy(() => import('pages/account/register'));
const Profile = lazy(() => import('pages/account/profile'));
const NotFound = lazy(() => import('pages/not-found'));

function PrivateRoute({ children, ...rest }) {
  return (
    <Route
      {...rest}
      render={({ location }) => (LocalStorage.has('ACCESS_TOKEN') ? (
        children
      ) : (
        <Redirect
          to={{
            pathname: '/login',
            state: { from: location },
          }}
        />
      ))}
    />
  );
}

PrivateRoute.propTypes = {
  children: PropTypes.node.isRequired,
};

export default function Routes() {
  return (
    <Router>
      <Header />
      <Suspense fallback={<Loading />}>
        <div className="body-page">
          <Switch>
            <Route exact path="/">
              <Home />
            </Route>
            <Route exact path="/login">
              <Login />
            </Route>
            <Route path="/register">
              <Register />
            </Route>
            <PrivateRoute path="/profile">
              <Profile />
            </PrivateRoute>
            <Route path="*">
              <NotFound />
            </Route>
          </Switch>
        </div>
      </Suspense>
    </Router>
  );
}
