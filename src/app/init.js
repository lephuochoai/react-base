import React from 'react';
import 'languages/i18n';

function Init() {
  React.useEffect(() => {
    const loading = document.getElementById('circle-loading');
    setInterval(() => {
      loading.style.display = 'none';
    }, 500);
  }, []);

  return null;
}

export default Init;
