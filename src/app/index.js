import { configureStore } from '@reduxjs/toolkit';
import rootReducer from 'features';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import Init from './init';
import './rootStyle.less';
import Routes from './routes';

import 'bootstrap/dist/css/bootstrap.min.css';

const store = configureStore({
  reducer: rootReducer,
});

ReactDOM.render(
  <Provider store={store}>
    <Init />
    <Routes />
  </Provider>,
  document.getElementById('root'),
);

if (module.hot) {
  module.hot.accept();
}
