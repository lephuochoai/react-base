import i18n from 'i18next';
import Backend from 'i18next-http-backend';
import LanguageDetector from 'i18next-browser-languagedetector';
import { reactI18nextModule } from 'react-i18next';
import Storage from 'utils/storage';

i18n.use(Backend).use(LanguageDetector).use(reactI18nextModule).init({
  backend: {
    /* translation file path */
    loadPath: '/public/locales/{{ns}}/{{lng}}.json',
  },
  detection: {
    order: ['querystring', 'cookie'],
    cache: ['cookie'],
  },
  fallbackLng: Storage.get('CURRENT_LANGUAGE') || 'vi',
  ns: 'translation',
  defaultNS: 'translation',
  debug: false,
  interpolation: {
    escapeValue: false,
  },
});

export default i18n;
