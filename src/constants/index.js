export default {
  LANGUAGES: [
    { code: 'en', languages: 'English' },
    { code: 'jp', languages: 'Japan' },
    { code: 'vi', languages: 'VietNam' },
  ],
};
